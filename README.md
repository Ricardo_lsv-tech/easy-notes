### EASY NOTES

### Introduction

It is a platform aimed at teachers who take the period or court notes, and calculate manually (with paper, calculator and pencil) the averages by subject and averages weighted. This app will allow the teacher to store the information on the subjects taught, together with the information
of the students enrolled in it, you can store the notes and the app will be able to calculate the total averages and by subject of each one of the students. In addition, the user can generate reports of the students with the best average and grade reports.

# Installation

- ptyhon3 -m venv env_name
- source env_name/bin/activate
- git init (if you don't have git installed)
- git clone git@gitlab.com:Ricardo_lsv-tech/easy-notes.git (by ssh)
- git clone https://gitlab.com/Ricardo_lsv-tech/easy-notes.git (by http)
- cd easynotes
- pip install -r requirements.txt
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

## MEMBERS
1. **ANDRES FELIPE MOLINARES BOLAÑOS**
2. **RICARDO ENRIQUE JARAMILLO ACEVEDO**
3. **LUIS ARMANDO TILVE SALGADO**
