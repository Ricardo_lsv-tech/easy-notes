from django.contrib import admin

from easynotesapp.models import Enrollment, Note, User, Student, Subject

# Register your models here.

@admin.register(User)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name','last_name', 'email', 'password')
    search_fields = ('first_name', 'email', 'last_name')
    list_filter = ('first_name', 'email')

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'document_type', 'document_number', 'date_created')
    search_fields = ('name', 'last_name', 'document_number')
    list_filter = ('name', 'document_number')

@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'teacher_id')
    search_fields = ('name', 'code')
    list_filter = ('code',)

@admin.register(Enrollment)
class EnrollmentAdmin(admin.ModelAdmin):
    list_display = ('grado', 'subject_id', 'student_id')
    search_fields = ('grado', 'student_id', 'subject_id')
    list_filter = ('grado',)

@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('student_id', 'subject_id', 'note_1', 'note_2', 'note_3')
    search_fields = ('student_id', 'subject_id')
    list_filter = ('student_id', 'subject_id')

#admin.site.register(Teacher)
#admin.site.register(Student)
#admin.site.register(Subject)
#admin.site.register(Enrollment)
#admin.site.register(Note)
