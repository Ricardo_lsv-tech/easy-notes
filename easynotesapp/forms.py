"""
Within this file the forms based on the application models were created
"""

from django import forms
from django.contrib.auth.forms import UserCreationForm 
from easynotesapp.models import Enrollment, Note, Student, Subject, User

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['name', 'last_name', 'document_type', 'document_number', 'teacher_id']
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Name'}))
    last_name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Last name'}))
    document_type = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Document type'}))
    document_number = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Document number'}))
    teacher_id = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=False), widget=forms.Select(attrs={'class': 'form-control my-2'}))

class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = ['name', 'code', 'teacher_id']
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Name'}))
    code = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Code'}))
    teacher_id = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=False), widget=forms.Select(attrs={'class': 'form-control my-2'}))

class EnrollmentForm(forms.ModelForm):
    class Meta:
        model = Enrollment
        fields = ['grado', 'subject_id', 'student_id']
    grado = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Grado'}))
    subject_id = forms.ModelChoiceField(label='Asignatura', queryset=Subject.objects.all(), widget=forms.Select(attrs={'class': 'form-control my-2'}))
    student_id = forms.ModelChoiceField(label='Estudiante', queryset=Student.objects.all(), widget=forms.Select(attrs={'class': 'form-control my-2'}))

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['student_id', 'subject_id', 'note_1', 'note_2', 'note_3', 'periodo']
    student_id = forms.ModelChoiceField(label='Estudiante', queryset=Student.objects.all(), widget=forms.Select(attrs={'class': 'form-control my-2'}))
    subject_id = forms.ModelChoiceField(label='Asignatura', queryset=Subject.objects.all(), widget=forms.Select(attrs={'class': 'form-control my-2'}))
    note_1 = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Nota 1'}))
    note_2 = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Nota 2'}))
    note_3 = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Nota 3'}))
    periodo = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': '1'}))

class UserForm(UserCreationForm ):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

    username = forms.CharField(label='Usuario', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Username'}))
    first_name = forms.CharField(label='Nombre', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'First name'}))
    last_name = forms.CharField(label='Apellidos', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Last name'}))
    email = forms.EmailField(label='Correo', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Email'}))
    password1 = forms.CharField(label='Contraseña', max_length=50, widget=forms.PasswordInput(attrs={'class': 'form-control my-2', 'placeholder': 'Password'}))
    password2 = forms.CharField(label='Confirmar contraseña', max_length=50, widget=forms.PasswordInput(attrs={'class': 'form-control my-2', 'placeholder': 'Confirm password'}))
    