from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    
    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Student(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    document_type = models.CharField(max_length=10)
    document_number = models.CharField(max_length=50)
    date_created = models.DateTimeField(auto_now_add=True)
    teacher_id = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} {self.last_name}'

class Subject(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    teacher_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name="list_teacher")

    def __str__(self):
        return f'{self.name}'
class Enrollment(models.Model):
    grado = models.CharField(max_length=50)
    subject_id = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name="subjectEnrollment")
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="studentEnrollment")

class Note(models.Model):
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="studentNote")
    subject_id = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name="subjectNote")
    note_1 = models.FloatField()
    note_2 = models.FloatField()
    note_3 = models.FloatField()
    periodo = models.IntegerField(default=1)

    def definition_note(self):
        return (self.note_1 + self.note_2 + self.note_3)/3

