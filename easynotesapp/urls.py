from django.urls import path
from easynotesapp import views
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.contrib.auth.decorators import login_required



urlpatterns = [
    path('', login_required(views.index), name='index'),
    path('students', login_required(views.students), name='students'),
    path('students/create', login_required(views.create_students), name='create_students'),
    path('students/edit/<int:id>', login_required(views.edit_students), name='edit_students'),
    path('students/delete/<int:id>', login_required(views.delete_students), name='delete_students'),
    path('subjects', login_required(views.subjects), name='subjects'),
    path('subjects/create', login_required(views.create_subjects), name='create_subjects'),
    path('subjects/edit/<int:id>', login_required(views.edit_subjects), name='edit_subjects'),
    path('subjects/delete/<int:id>', login_required(views.delete_subjects), name='delete_subjects'),
    path('notes/', login_required(views.notes), name='notes'),
    path('notes/create', login_required(views.create_notes), name='create_notes'),
    path('notes/edit/<int:id>', login_required(views.edit_notes), name='edit_notes'),
    path('enrollments', login_required(views.enrollments), name='enrollments'),
    path('enrollments/create', login_required(views.create_enrollments), name='create_enrollments'),
    path('enrollments/edit/<int:id>', login_required(views.edit_enrollments), name='edit_enrollments'),
    path('enrollments/delete/<int:id>', login_required(views.delete_enrollments), name='delete_enrollments'),
    path('teachers', login_required(views.user_list), name='teachers'),
    path('teachers/create', login_required(views.create_user), name='create_user'),
    path('teachers/edit/<int:id>', login_required(views.edit_user), name='edit_user'),
    path('teachers/delete/<int:id>', login_required(views.delete_user), name='delete_user'),
    path('accounts/login/', LoginView.as_view(template_name='pages/login.html'), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('accounts/password_reset/', PasswordResetView.as_view(template_name='pages/registration/password_reset_form.html', html_email_template_name='pages/registration/password_reset_email.html'), name='password_reset'),
    path('accounts/password_reset/done/', PasswordResetDoneView.as_view(template_name='pages/registration/password_reset_done.html'), name='password_reset_done'),
    path('accounts/password_reset/confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(template_name='pages/registration/password_reset_confirm.html'), name='password_reset_confirm'),
    path('accounts/password_reset/complete/', PasswordResetCompleteView.as_view(template_name='pages/registration/password_reset_complete.html'), name='password_reset_complete'),

]