from django.shortcuts import redirect, render
from django.urls import reverse
from easynotesapp.forms import EnrollmentForm, NoteForm, StudentForm, SubjectForm, UserForm
from easynotesapp.models import Enrollment, Note, Student, Subject, User


# Create your views here.
def index(request):
    return render(request, 'pages/index.html')

'''
CRUD STUDENTS
'''
def students(request):
    list_student = Student.objects.all()
    user = request.user
    if not user.is_staff:
        list_student = Student.objects.filter(teacher_id=user)
    return render(request, 'students/index.html',  {'list_student': list_student})

def create_students(request):
    user = request.user
    if user.is_staff:
        if request.method == 'POST':
            form = StudentForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(reverse('students'))
        else:
            form = StudentForm()
        return render(request, 'students/create.html', {'form': form})
    else:
        return redirect(reverse('index'))

def edit_students(request, id):
    user = request.user
    if user.is_staff:
        if request.method == 'GET':
            student = Student.objects.get(id=id)
            form = StudentForm(instance=student)
            return render(request, 'students/edit.html', {'student': student, 'form': form})
        elif request.method == 'POST':
            student = Student.objects.get(pk=id)
            form = StudentForm(request.POST, instance=student)
            if form.is_valid():
                form.save()
                return redirect(reverse('students'))
    else:
        return redirect(reverse('index'))

def delete_students(request, id):
    user = request.user
    if user.is_staff:
        student = Student.objects.get(id=id)
        student.delete()
        return redirect(reverse('students'))
    else:
        return redirect(reverse('index'))

'''
CRUD SUBJECTS
'''
def subjects(request):
    list_subject = Subject.objects.all()
    user = request.user
    if not user.is_staff:
        list_subject = Subject.objects.filter(teacher_id=user)
    return render(request, 'subjects/index.html',  {'list_subject': list_subject})

def create_subjects(request):
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('subjects'))
    else:
        form = SubjectForm()
    return render(request, 'subjects/create.html', {'list_subject': form})

def edit_subjects(request, id):
    user = request.user
    if user.is_staff:
        if request.method == 'GET':
            subject = Subject.objects.get(id=id)
            form = SubjectForm(instance=subject)
            return render(request, 'subjects/edit.html', {'subject': subject, 'form': form})
        elif request.method == 'POST':
            subject = Subject.objects.get(pk=id)
            form = SubjectForm(request.POST, instance=subject)
            if form.is_valid():
                form.save()
                return redirect(reverse('subjects'))
    else:
        return redirect(reverse('index'))

def delete_subjects(request, id):
    user = request.user
    if user.is_staff:
        subject = Subject.objects.get(id=id)
        subject.delete()
        return redirect(reverse('subjects'))
    else:
        return redirect(reverse('index'))
'''
CRUD MATRICULAS
'''
def enrollments(request):
    user = request.user
    if user.is_staff:
        list_enrollment = Enrollment.objects.all()
        user = request.user
        if not user.is_staff:
            list_enrollment = Enrollment.objects.filter(subject_id__teacher_id=user)
        return render(request, 'enrollments/index.html',  {'list_enrollment': list_enrollment})
    else:
        return redirect(reverse('index'))

def create_enrollments(request):
    user = request.user
    if user.is_staff:
        if request.method == 'POST':
            form = EnrollmentForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(reverse('enrollments'))
        else:
            form = EnrollmentForm()
        return render(request, 'enrollments/create.html', {'form': form})
    else:
        return redirect(reverse('index'))

def edit_enrollments(request, id):
    user = request.user
    if user.is_staff:
        if request.method == 'GET':
            enrollment = Enrollment.objects.get(id=id)
            form = EnrollmentForm(instance=enrollment)
            return render(request, 'enrollments/edit.html', {'enrollment': enrollment, 'form': form})
        elif request.method == 'POST':
            enrollment = Enrollment.objects.get(pk=id)
            form = EnrollmentForm(request.POST, instance=enrollment)
            if form.is_valid():
                form.save()
                return redirect(reverse('enrollments'))
    else:
        return redirect(reverse('index'))

def delete_enrollments(request, id):
    user = request.user
    if user.is_staff:
        enrollment = Enrollment.objects.get(id=id)
        enrollment.delete()
        return redirect(reverse('enrollments'))
    else:
        return redirect(reverse('index'))
'''
CRUD NOTES
'''
def notes(request):
    periodo = request.POST.get('periodo')
    if periodo:
        list_notes = Note.objects.filter(periodo=request.POST.get('periodo'))
        user = request.user
        if not user.is_staff:
            list_notes = Note.objects.filter(subject_id__teacher_id=user, periodo=periodo)
    else:
        list_notes = Note.objects.all()
        user = request.user
        if not user.is_staff:
            list_notes = Note.objects.filter(subject_id__teacher_id=user)
    return render(request, 'notes/index.html',  {'list_notes': list_notes})

def create_notes(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('notes'))
    else:
        form = NoteForm()
    return render(request, 'notes/create.html', {'form': form})

def edit_notes(request, id):
    if request.method == 'GET':
        note = Note.objects.get(id=id)
        form = NoteForm(instance=note)
        return render(request, 'notes/edit.html', {'note': note, 'form': form})
    elif request.method == 'POST':
        note = Note.objects.get(pk=id)
        form = NoteForm(request.POST, instance=note)
        if form.is_valid():
            form.save()
            return redirect(reverse('notes'))

'''
CRUD USERS
'''
def user_list(request):
    user = request.user
    if user.is_staff:
        users = User.objects.all()
        return render(request, 'teachers/index.html', {'users': users})
    else:
        return redirect(reverse('index'))


def create_user(request):
    user = request.user
    if user.is_staff:
        if request.method == 'POST':
            form = UserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(reverse('teachers'))
        else:
            form = UserForm()
        return render(request, 'teachers/create.html', {'form': form})
    else:
        return redirect(reverse('index'))


def edit_user(request, id):
    user = request.user
    if user.is_staff:
        if request.method == 'GET':
            user = User.objects.get(id=id)
            form = UserForm(instance=user)
            return render(request, 'teachers/edit.html', {'user': user, 'form': form})
        elif request.method == 'POST':
            user = User.objects.get(pk=id)
            form = UserForm(request.POST, instance=user)
            if form.is_valid():
                form.save()
                return redirect(reverse('teachers'))
    else:
        return redirect(reverse('index'))


def delete_user(request, id):
    user = request.users
    if user.is_staff:
        user = User.objects.get(id=id)
        user.delete()
        return redirect(reverse('teachers'))
    else:
        return redirect(reverse('index'))
    